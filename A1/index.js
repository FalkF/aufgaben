class Header extends React.PureComponent {
    static propTypes = {
        title: PropTypes.string.isRequired,
        // ...
    };

    render() {
        return (
            <header className="header">
                <h1>Todos</h1>
                {/* ... */}
            </header>
        );
    }
}

class App extends React.PureComponent {
    render() {
        return (
            <Header title="Todos"/>
            // ...
        );
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById("root"));
