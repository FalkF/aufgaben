import React, {PureComponent} from 'react';
import './App.css';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';

export default class App extends PureComponent {
    render() {
        return (
            <React.Fragment>
                <Header title="Todos"/>
                <Footer />
            </React.Fragment>
        );
    }
}
